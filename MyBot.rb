$:.unshift File.dirname($0)
require 'ants.rb'

ai=AI.new

ai.setup do |ai|
	# your setup code here, if any
end

file = File.open("debug.txt", "w+")

#file.write("X: " + square.row.to_s + " ")
#file.write("Y: " + square.col.to_s + " ")
#file.puts("X: " + square.row.to_s + " " "Y: " + square.col.to_s)
#file.write("land?: " + square.land?.to_s + " ")
#file.write("water?: " + square.water?.to_s + " ")
#file.write("food?: " + square.food?.to_s + " ")
#file.write("ant?: " + square.ant?.to_s + " ")
#file.write("\n")

explored = []
directions = [:S, :E, :N, :W]

ai.run do |ai|
	# your turn code here
  file.puts("TURN: " + ai.turn_number.to_s)

	food_locations = []
	turn_moves = []

  ai.map.each do |row|
  	row.each do |square|
			food_locations << square if square.food?
  	end
  end
  explored.each do |e|
  	explored.delete(e) if (ai.turn_number - e[:turn]) > 10
  end

	ai.my_ants.each do |ant|
		explored << {:square => ant.square, :turn => ai.turn_number}

=begin
		food_locations.each do |food|
			if ((food.col - ant.col).abs < 4)
				if (food.col - ant.col) > 0
					ant.order :N if possible_directions.include?(:N)
					turn_moves << ant.square.neighbor(:N)
					moved = true
				else
					ant.order :S if possible_directions.include?(:S)
					turn_moves << ant.square.neighbor(:S)
					moved = true
				end
			end
			break
			if ((food.row - ant.row).abs < 4)
				if (food.row - ant.row) > 0
					ant.order :W if possible_directions.include?(:W)
					turn_moves << ant.square.neighbor(:W)
					moved = true
				else
					ant.order :E if possible_directions.include?(:E)
					turn_moves << ant.square.neighbor(:E)
					moved = true
				end
			end			
		end

		break if moved
=end

		directions.each do |dir|
			if ant.square.neighbor(dir).land? && !turn_moves.include?(ant.square.neighbor(dir)) && !explored.any?{|e| ant.square.neighbor(dir) == e[:square]}
				ant.order dir
				turn_moves << ant.square.neighbor(dir)
				break
			end
		end
	end
end

file.close
